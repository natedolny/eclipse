# Eclipse 

Inspired by the mesmerizing beauty of solar eclipses, the colour scheme casts 
a spellbinding aura over your coding universe, merging the darkness of space 
with the brilliance of celestial illumination.

## Screenshots 


## Colour Palette

![colour palette](</img/colours.png>)


## Installation

1. Open a terminal 

2. Navigate to the directory where you downloaded eclipse

3. Copy eclipse.vim to your vim colours directory. This is typically located at 
~/.vim/colors/ 

4. Open your vim configuration file in a text editor 

5. Set eclipse to be your chosen colour scheme 

```
colorscheme eclipse
```

6. Save the changes to the configuration file 

7. apply changes and enjoy the spellbinding aura of eclipse

vim:

```
:source ~/.vimrc
```

You have successfully installed eclipse! Enjoy coding with the mesmerizing 
blend of cosmic beauty. 

