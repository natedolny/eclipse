"Name: Eclipse 
"Version: 1.0.0-alpha
"Maintainer: gitlab.com/natedolny



" colour palette 

let s:black	  = {"gui": "#333533", "cterm": "236"}
let s:grey 		= {"gui": "#D6D6D6", "cterm": "252"}
let s:yellow  = {"gui": "#FFEE32", "cterm": "227"}

" Background highlighting 
hi Normal ctermbg=black ctermfg=grey

" Comment highlighting 
hi Comment ctermfg=grey 

" String highlighting 
hi String ctermfg=grey

" Type highlighting 
hi Type ctermfg=yellow

" Keyword highlighting
hi Keyword ctermfg=yellow


